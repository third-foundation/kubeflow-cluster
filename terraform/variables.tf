variable "project_name" {
    type = string
    description = "Project name."
}

variable "project_region" {
    type = string
    description = "The GCP region of VW Machines."
}

variable "machine_type" {
    type = string
    description = "Size type Machine."
}

variable "vm-instance-prefix" {
    type = string
    description = "VM resource prefix id."
}

variable "generic-user" {
    type = string
    description = "generic user to access instance."
}

variable "linux-disto" {
    type = string
    description = "Distribuição linux usada na imagem da máquina."
}

variable "disto-version" {
    type = string
    description = "Versão da distribuição linux."
}

variable "disk-size" {
    type = number
    description = "Tamanho do disco em Gb."
}

variable "ports2open" {
    type = list(string)
    description = "Firewall ports to open."
}

variable "firewall-rule-name" {
    type = string
    description = "Firewall rule name for jenkins server."
}

variable "number_instances" {
    type = string
    description = "Number os machines on the cluster."
}
