output "ip_master" {
 value = "${google_compute_instance.instance[0].network_interface.0.access_config.0.nat_ip}"
}
output "ip_host1" {
 value = "${google_compute_instance.instance[1].network_interface.0.access_config.0.nat_ip}"
}

output "ip_host2" {
 value = "${google_compute_instance.instance[2].network_interface.0.access_config.0.nat_ip}"
}