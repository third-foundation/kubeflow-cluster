// Provedor do serviço
provider "google" {
  credentials = "${file("../credentials/credentials.json")}"
  project     = "${var.project_name}"
  region      = "${var.project_region}"
}

resource "google_compute_instance" "instance" {
  name         = "${var.vm-instance-prefix}-${var.project_name}-${count.index}"
  machine_type = "${var.machine_type}"
  zone         = "${var.project_region}"
  count        = "${var.number_instances}"

  boot_disk {
    initialize_params {
      image = "${var.linux-disto}/${var.disto-version}"
      size  = "${var.disk-size}"
    }

  }

  lifecycle {
    ignore_changes = ["metadata_startup_script"]
  }

  // Instalação de requisitos mínimos para o ambiente de prototipagem
  metadata_startup_script = "${file("./src/init.sh")}"

  network_interface {
    network = "default"

    access_config {
      // Include this section to give the VM an external ip address
    }
  }

  metadata = {
    ssh-keys = "${var.generic-user}:${file("../credentials/id_rsa.pub")}"
  }

}

// Cria regra de firewall
resource "google_compute_firewall" "firewall" {
 name    = "${var.firewall-rule-name}"
 network = "default"

 allow {
   protocol = "tcp"
   ports = "${var.ports2open}"
 }
}
